# Sistema de Reservas de Hotel

O Sistema de Reservas de Hotel é um projeto prático que simula um sistema de reservas de hotel real. Ele aplica os conceitos de herança, polimorfismo, abstração e encapsulamento.

## Classes

O sistema contém as seguintes classes:

- **Quarto**: Esta classe representa um quarto no hotel. Ela contém atributos como número do quarto, tipo de quarto, preço, status (disponível/ocupado), etc.

- **Cliente**: Esta classe representa um cliente. Ela contém atributos como nome do cliente, ID do cliente, reservas realizadas, etc.

- **Reserva**: Esta classe representa uma reserva. Ela contém atributos como ID da reserva, quarto reservado, cliente, data de check-in, data de check-out, etc.

## Funcionalidades

O sistema fornece as seguintes funcionalidades:

- **Reserva de quartos**: Os clientes podem reservar quartos disponíveis. O sistema registra a reserva e atualiza o status do quarto.

- **Check-in**: Os clientes podem fazer check-in em suas reservas. O sistema registra o check-in e atualiza o status do quarto para ocupado.

- **Check-out**: Os clientes podem fazer check-out de suas reservas. O sistema registra o check-out e atualiza o status do quarto para disponível.

- **Gerenciamento de clientes**: O sistema permite adicionar, atualizar e remover clientes.

- **Gerenciamento de quartos**: O sistema permite adicionar, atualizar e remover quartos.

## Como usar

1. Inicie o sistema.
2. Selecione uma opção do menu principal.
3. Siga as instruções na tela para usar cada funcionalidade.