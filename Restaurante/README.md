# Sistema de Gerenciamento de Restaurante

O Sistema de Gerenciamento de Restaurante é um projeto prático que simula um sistema de restaurante real. Ele aplica os conceitos de herança, polimorfismo, abstração e encapsulamento.

## Classes

O sistema contém as seguintes classes:

- **Prato**: Esta classe representa um prato no menu do restaurante. Ela contém atributos como nome do prato, ID do prato, preço, etc.

- **Pedido**: Esta classe representa um pedido feito por um cliente. Ela contém atributos como ID do pedido, pratos pedidos, total do pedido, etc.

- **Cliente**: Esta classe representa um cliente. Ela contém atributos como nome do cliente, ID do cliente, pedidos realizados, etc.

## Funcionalidades

O sistema fornece as seguintes funcionalidades:

- **Fazer pedidos**: Os clientes podem fazer pedidos de pratos do menu. O sistema registra o pedido e atualiza a lista de pedidos.

- **Visualização de pedidos**: Os clientes podem visualizar seus pedidos. O sistema retorna uma lista de pedidos para o cliente.

- **Gerenciamento de clientes**: O sistema permite adicionar, atualizar e remover clientes.

- **Gerenciamento de pratos**: O sistema permite adicionar, atualizar e remover pratos do menu.

## Como usar

1. Inicie o sistema.
2. Selecione uma opção do menu principal.
3. Siga as instruções na tela para usar cada funcionalidade.